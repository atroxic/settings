inoremap jk <ESC>
syntax on
set tabstop=4
set shiftwidth=4
set smartindent
set expandtab
set rnu
set number

au BufRead,BufNewFile *.vue set syntax=html
