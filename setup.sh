#!/bin/bash

FILE_LIST=(".bash_git" ".bashrc" ".gitconfig" ".profile" ".vimrc" ".git-bash_completion.bash")
for file in ${FILE_LIST[@]}; do
	if [ -e "$HOME/$file" ]; then
		if [ -L "$HOME/$file" ]; then
			#File Exists and is SymLink
			if [ `readlink -f $file` == "$PWD/$file" ]; then
				echo "Symlink already exists"
			else
				echo "Symlink exists but points wrong. Rewriting"
				rm "$HOME/$file"
				ln -s "$PWD/$file" "$HOME/$file"
			fi
		elif [ -f "$HOME/$file" ]; then
			#Regular File, not symLink
			echo "Regular File, not symlink, backing up"
			mv "$HOME/$file" "$HOME/$file.bak"
			ln -s "$PWD/$file" "$HOME/$file"
		fi
	elif [ -L "$HOME/$file" ]; then
		#File exists but bad symlink
		echo "File exists but bad symlink, backing up symlink"
		mv "$HOME/$file" "$HOME/$file.bak"
		ln -s "$PWD/$file" "$HOME/$file"
	else
		#File not exist
		echo "File not exist, creating symlink"
		ln -s "$PWD/$file" "$HOME/$file"
	fi
done

echo "Reloading Bash"
exec bash
